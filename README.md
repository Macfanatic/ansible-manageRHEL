# Manage RHEL & CentOS

## Function
A collection of Ansible Playbooks used to perform common tasks outside of a role

## Tested Environment(s)
RHEL 7 & CentOS 7

## Changelog

**v1.0 (2017-05-04)**
* Original Release
